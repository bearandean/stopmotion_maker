#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 30 21:11:41 2020

@author: andres
"""
import os
import re
import argparse
import numpy as np
import cv2
import glob


def natural_sort(mylist):
    """ natural_sort; Sorts the given list in the way that usual humans expect.
    Input:
    . mylist        : a list to be sorted in a human-like way
    Outpt:
    . mylist        : same list, but sorted in a 'natural' way
    """
    # TODO: Fix this to conform to PEP 8. PEP 8: do not assign a lambda expression, use a def
    convert = lambda text: int(text) if text.isdigit() else text
    alphanum_key = lambda key: [convert(c) for c in re.split('([0-9]+)', key)]
    mylist.sort(key=alphanum_key)
    return mylist

 
if __name__ == "__main__":

    # module description
    parser = argparse.ArgumentParser(description="makes a stop motion movie from input jpg frames")
    parser.add_argument("--imgsdir", help="path to imgs", type=str)
    # optional var
    parser.add_argument("--y_bandwidth", nargs="?", help="band of pixels around the cylinder axis to consider dot contours", type=int, default=150)
    parser.add_argument("--max_dot_diameter", nargs="?", help="max dot diameter for dot-related contour filtering", type=int, default=100)

    # parse arguments
    args=parser.parse_args()
    # global vars
    imgsdir = args.imgsdir
    if imgsdir is None:
        parser.print_usage()
        exit(0)
    
    #imgsdir = "/home/andres/Pictures/Wilma/Wilma and the many vegetables/"
    img_array = []
    imgslist = glob.glob(imgsdir+"*.JPG")
    for filename in natural_sort(imgslist):
        img = cv2.imread(filename)
        height, width, layers = img.shape
        size = (width,height)
        img_array.append(img)

    out = cv2.VideoWriter(imgsdir+'project_output.avi',cv2.VideoWriter_fourcc(*'DIVX'), 5, size)
     
    for i in range(len(img_array)):
        out.write(img_array[i])
    out.release()